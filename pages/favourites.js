
import React, { Component } from 'react';
import Header from '../components/header';
import Favourite from '../components/favourite';
import Footer from '../components/footer';
import Vidget from '../components/vidget';

export default class FavouritesPage extends Component {

	render() {

		return (
			<>
				<Header />
				<Favourite />
				<Footer />
				<Vidget />
			</>
		);
	};
};