
import React, { Component } from 'react';
import Header from '../components/header';
import Confidential from '../components/confidential';
import Footer from '../components/footer';
import Vidget from '../components/vidget';

export default class ConfidentialPage extends Component {

	render() {

		return (
			<>
				<Header />
				<Confidential />
				<Footer />
				<Vidget />
			</>
		);
	};
};