
import React, { Component } from 'react';
import Header from '../components/header';
import Cart from '../components/cart';
import Footer from '../components/footer';
import Vidget from '../components/vidget';

export default class CartPage extends Component {

	render() {

		return (
			<>
				<Header />
				<Cart />
				<Footer />
				<Vidget />
			</>
		);
	};
};