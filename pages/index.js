
import React, { Component } from 'react';
import Intro from '../components/intro';
import Trust from '../components/trust';
import Video from '../components/video';
import Examples from '../components/examples';
import Exx from '../components/exx';
import Catalog from '../components/catalog';
import CatalogTab from '../components/catalog-tab';
import Bonuses from '../components/bonuses';
import GallerySwitch from '../components/gallery-switch';
import Ceo from '../components/ceo';
import Request from '../components/request';
import Footer from '../components/footer';
import Vidget from '../components/vidget';

export default class HomePage extends Component {

	render() {

		return (
			<>
				<Intro />
				<Trust />
				<Video />
				<Examples />
				<Exx />
				<Catalog />
				<CatalogTab />
				<Bonuses />
				<GallerySwitch />
				<Ceo />
				<Request />
				<Footer />
				<Vidget />
			</>
		);
	};
};