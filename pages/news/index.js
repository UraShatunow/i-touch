
import React, { Component } from 'react';
import Header from '../../components/header';
import Tidings from '../../components/tidings';
import Footer from '../../components/footer';
import Vidget from '../../components/vidget';

export default class NewsPage extends Component {

	render() {

		return (
			<>
				<Header />
				<Tidings />
				<Footer />
				<Vidget />
			</>
		);
	};
};