import React from 'react';
import NewsServiсe from '../../../services/news-service';
import Header from '../../../components/header';
import Footer from '../../../components/footer';

const NewsContent = (props) => {
	return (
		<>
			<Header />
			<div>
				<div>
					<div>

						{props.news.id}

					</div>
				</div>
			</div>
			<Footer />
		</>
	);
}

export const getStaticPaths = async function () {

	const newsService = new NewsServiсe();

	const cards = newsService.newsN;

	return {
		paths: cards.map((item) => (
			{
				params: {
					slug: item.id.toString(),
				}
			}
		)),
		fallback: 'blocking'
	}
}

export const getStaticProps = async (context) => {
	const { slug } = context.params;
	const newsService = new NewsServiсe();

	const cards = newsService.newsN;

	const news = cards.find((found) => found.id.toString() === slug);

	console.log(news, slug);

	if (!news) {
		return {
			notFound: true
		}
	}

	return {
		props: {
			news
		}
	}
}

export default NewsContent;