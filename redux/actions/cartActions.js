import {
	INITIALYZE_CARDS,
	ADD_ITEM_TO_CART,
	INC_ITEM_COUNT,
	DELETE_ITEM,
	UPDATE_ITEM
} from '../types/cartTypes';

export const initialyzeCards = cardsArr => dispatch => {
	dispatch({ type: INITIALYZE_CARDS, payload: cardsArr })
}

export const addItemToCart = item => dispatch => {
	dispatch({ type: ADD_ITEM_TO_CART, payload: item });
};

export const incItemCount = item => dispatch => {
	dispatch({ type: INC_ITEM_COUNT, payload: item });
};

export const delItem = item => dispatch => {
	dispatch({ type: DELETE_ITEM, payload: item })
};

export const updateItem = item => dispatch => {
	dispatch({ type: UPDATE_ITEM, payload: item })
};