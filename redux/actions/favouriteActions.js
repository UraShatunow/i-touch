import { ADD_FAVORITE, DELETE_FAVORITE } from '../types/favouriteTypes';

export const addFavouriteItem = item => dispatch => {
	dispatch({ type: ADD_FAVORITE, payload: { item } });
};

export const deleteFavouriteItem = item => dispatch => {
	dispatch({ type: DELETE_FAVORITE, payload: { item } });
};