import {
	INITIALYZE_CARDS,
	ADD_ITEM_TO_CART,
	INC_ITEM_COUNT,
	DELETE_ITEM, 
	UPDATE_ITEM
} from '../types/cartTypes';

const initialState = {
	cards: [],
	cart: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case INITIALYZE_CARDS:
			return {
				...state,
				cards: action.payload
			}

		case INC_ITEM_COUNT:
			return {
				...state,
				cart: state.cart.map(item =>
					item.id === action.payload.id ? action.payload : item)
			}
		
		case DELETE_ITEM: 
			return {
				...state,
				cart: state.cart.filter(item => item.id !== action.payload.id)
			};
	
		case ADD_ITEM_TO_CART:
			return {
				...state,
				cart: [...state.cart, action.payload]
			}

		case UPDATE_ITEM:
			return {
				...state,
				cart: state.cart.map(item =>
					item.id === action.payload.id ? action.payload : item)
			}

		default:
			return state;
	}
};

export default reducer;
