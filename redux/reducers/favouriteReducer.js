import {
	ADD_FAVORITE,
	DELETE_FAVORITE
} from '../types/favouriteTypes';

const initialState = {
	favourites: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case ADD_FAVORITE:
			return {
				...state,
				favourites: [...state.favourites, action.payload.item]
			};
		case DELETE_FAVORITE:
			return {
				...state,
				favourites: state.favourites.filter(item => item.id !== action.payload.item.id)
			};
		default:
			return state;
	}
};

export default reducer;
