import { combineReducers } from 'redux';
import catalogReducer from './catalogReducer';
import favouriteReducer from './favouriteReducer';

const rootReducer = combineReducers({
	catalogState: catalogReducer,
	favouriteState: favouriteReducer
});

export default rootReducer;