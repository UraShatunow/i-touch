import React, { useState, useEffect } from 'react';
import styles from './tidings.module.css';
import Link from 'next/link';
import NewsServiсe from '../../services/news-service';

const Tidings = () => {

	const [allCards, setAllCards] = useState([]);

	const newsService = new NewsServiсe();

	useEffect(() => {
		newsService.getNews()
			.then((data) => {
				setAllCards(data)
			})
	}, [])

	console.log(allCards);

	const content = allCards.map((item) => {
		return (
			<Link href={{ pathname: `/news/${item.id}` }} passHref key={item.id}>
				<a href="passed" className={styles.link}>
					<div className={styles.card}>
						<div className={styles.pic}>
							<picture>
								<source srcSet={item.imgUrlj} type="image/webp" />
								<img className={styles.img} src={item.imgUrl} alt='product' />
							</picture>
						</div>
						<h3 className={styles.heading}>{item.heading}</h3>
						<p className={styles.date}>{item.date}</p>
					</div>
				</a>
			</Link>
		)
	})

	return (
		<footer className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<h3 className={styles.title}>Новости</h3>

					<div className={styles.news}>
						{content}
					</div>

				</div>
			</div>
		</footer>
	);
}

export default Tidings;