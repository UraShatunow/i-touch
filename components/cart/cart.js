import styles from './cart.module.css';
import Link from 'next/link';
import Product from '../product';
import { useSelector } from 'react-redux';
import Card from '../card/card';
import CardServiсe from '../../services/card-serviсe';
import { useEffect, useState } from 'react';
import cn from 'classnames';

const Cart = () => {

	const [allCards, setAllCards] = useState([]);
	const [type, setType] = useState('personal');

	const cardService = new CardServiсe();

	useEffect(() => {
		cardService.getCards()
			.then((data) => {
				setAllCards(data)
			})
	}, [])

	const cart = useSelector(state => state.catalogState.cart);

	const createCards = (item) => {
		let foundItem = cart.find((found) => found.id === item.id);

		if (foundItem) {
			return (<Card
				key={foundItem.id}
				card={foundItem} />)
		}

		return (<Card
			key={item.id}
			card={item} />)
	}

	const cartCards = cart.map((i) => {
		return (
			<Product
				key={i.id}
				cartItem={i}
			/>
		)
	})

	const checkId = (i) => {
		if (i == 1 || i == 2 || i == 13) {
			return i
		}

		return 0
	}

	const content = allCards.filter(i => i.type == type).map((item) => { return createCards(item) });
	const recommended = allCards.filter(i => i.id == checkId(i.id)).map((item) => { return createCards(item) });

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<Link href="/" passHref>
						<a href="passed" className={styles.home_link}>
							<img src='/images/arrow-left.svg' alt='arrow' />
							Вернуться на главную
						</a>
					</Link>

					<section className={styles.section}>

						<div className={styles.cart}>
							<h2>Корзина</h2>

							{cart.length < 1 && (
								<div className={styles.empty}>
									<h3>Ваша корзина пуста :(</h3>
									<p>Исправить это просто: выберите в каталоге интереусющий товар <br />
										и нажмите кнопку «В корзину»</p>
									<Link href={{ pathname: '/', hash: "#catalog" }} passHref>
										<a href="passed" className={styles.to_catalog}>
											Перейти в каталог
										</a>
									</Link>
								</div>
							)}

							<div className={styles.cart_inner}>
								{cartCards}
							</div>
						</div>


						<div className={styles.bonuses}>
							<h3>Бонусы для клиентов!</h3>
							<div className={styles.text}>
								<div className={styles.group}>
									<div className={styles.bonus}><span className={styles.round}></span><p><span className={styles.blue}>Бесплатная доставка</span> в будние дни</p></div>
									<div className={styles.bonus}><span className={styles.round}></span><p>При заказе разработки ПО — аренда плазмы <span className={styles.blue}>в подарок</span></p></div>
								</div>
								<div className={styles.group}>
									<div className={styles.bonus}><span className={styles.round}></span><p>При заказе от 2-х столов доставка <span className={styles.blue}>бесплатная</span></p></div>
									<div className={styles.bonus}><span className={styles.round}></span><p>Персональный менеджер на связи <span className={styles.blue}>24 часа в сутки</span></p></div>
								</div>
							</div>
						</div>

					</section>

					{cart.length === 0 || (
						<div className={styles.extra}>
							<h3 className={styles.heading}>Дополнительно</h3>
							<div className={styles.switches}>
								<button className={cn(styles.switch, {
									[styles.switch_active]: type == 'personal'
								})} onClick={() => {
									setType('personal')
								}}>Сопровождающий персонал</button>
								<button className={cn(styles.switch, {
									[styles.switch_active]: type == 'brand'
								})} onClick={() => {
									setType('brand')
								}}>Брендирование</button>
							</div>
							<div className={styles.extra_cards}>
								{content}
							</div>
						</div>
					)}

					{cart.length !== 0 || (
						<div className={styles.recommended}>
							<h3 className={styles.heading}>Рекомендуем вам</h3>
							<div className={styles.extra_cards}>
								{recommended}
							</div>
						</div>
					)}

				</div>
			</div>
		</div>

	);
}

export default (Cart);