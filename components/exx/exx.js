import React, { Component } from 'react';
import styles from './exx.module.css';
import Tab from '../Tabs/Tab';
import TabNav from '../Tabs/TabNav';

import { Swiper, SwiperSlide } from 'swiper/react';

import SwiperCore, {
	Navigation,
	Pagination
} from 'swiper';

SwiperCore.use([Navigation, Pagination]);

export default class Exx extends Component {

	constructor(props) {
		super(props);

		this.state = {
			selected: "Mercedes-Benz"
		}
	}

	setSelected = (tab) => {
		this.setState({ selected: tab });
	}

	render() {



		return (
			<div className={styles.content}>
				<div className={styles.container}>
					<div className="exx_inner">

						<h2 className={styles.title}>Примеры работ</h2>

						<TabNav tabs={['Mercedes-Benz', '100 лет инсулину']}
							selected={this.state.selected}
							setSelected={this.setSelected}>
							<Tab isSelected={this.state.selected === 'Mercedes-Benz'}>
								
								<div className={styles.exx_swiper_wrapper}>
									<Swiper
										modules={[Pagination, Navigation]}
										slidesPerView={1}
										pagination={{
											"clickable": true
										}}
										navigation={true}
										centeredSlides={true}
										className="swiper_exx"
									>

										<SwiperSlide>
											<div className={styles.slide_img}>
												<picture>
													<source srcSet='/images/exx-mercedes-small-1.jpg' media="(max-width: 767px)" />
													<img className={styles.img} src='/images/exx-mercedes-1.webp' alt="example" />
												</picture>
											</div>
										</SwiperSlide>

										<SwiperSlide>
											<div className={styles.slide_img}>
												<picture>
													<source srcSet='/images/exx-mercedes-small-2.jpg' media="(max-width: 767px)" />
													<img className={styles.img} src='/images/exx-mercedes-2.webp' alt="example" />
												</picture>
											</div>
										</SwiperSlide>

									</Swiper>
								</div>

								<div className={styles.slide_text}>
									<h3 className={styles.heading}>Сбор контактов гостей на выставке Мерседес</h3>
									<p>За 1 минуту гостю нужно сопоставить кадры с названиями фильмов.
										После выполнения на экране появляется форма для контактных данных.
										Время на одного гостя: до 2 минут</p>
								</div>

							</Tab>



							<Tab isSelected={this.state.selected === '100 лет инсулину'}>
								
								<Swiper
									modules={[Pagination, Navigation]}
									slidesPerView={1}
									pagination={{
										"clickable": true
									}}
									navigation={true}
									centeredSlides={true}
									className="swiper_exx"
								>

									<SwiperSlide>
										<div className={styles.slide_img}>
											<picture>
												<source srcSet='/images/exx-insulin-small-1.jpg' media="(max-width: 767px)" />
												<img className={styles.img} src='/images/exx-insulin-1.webp' alt="example" />
											</picture>
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.slide_img}>
											<picture>
												<source srcSet='/images/exx-insulin-small-2.jpg' media="(max-width: 767px)" />
												<img className={styles.img} src='/images/exx-insulin-2.webp' alt="example" />
											</picture>
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.slide_img}>
											<picture>
												<source srcSet='/images/exx-insulin-small-3.jpg' media="(max-width: 767px)" />
												<img className={styles.img} src='/images/exx-insulin-3.webp' alt="example" />
											</picture>
										</div>
									</SwiperSlide>

								</Swiper>

								<div className={styles.slide_text}>
									<h3 className={styles.heading}>Тут про 100 лет инсулину</h3>
									<p>За 1 минуту гостю нужно сопоставить кадры с названиями фильмов.
										После выполнения на экране появляется форма для контактных данных.
										Время на одного гостя: до 2 минут</p>
								</div>

							</Tab>

						</TabNav>

					</div>
				</div>
			</div>
		);
	}
}
