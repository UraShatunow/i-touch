import { useState, useEffect } from 'react';
import styles from './catalog.module.css';
import cn from 'classnames';
import Card from '../card/card';
import ShowMore from '../show-more';
import { useSelector, useDispatch } from 'react-redux';
import { initialyzeCards } from '../../redux/actions/cartActions';
import CardServiсe from '../../services/card-serviсe';
import useWindowSize from '../../hooks/useWindowSize';

const Catalog = () => {

	const cart = useSelector(state => state.catalogState.cart);
	const cards = useSelector(state => state.catalogState.cards);
	const arrLength = useWindowSize() < 1351 & useWindowSize() > 1022 ? 6 : 8;
	const incValue = useWindowSize() < 1351 & useWindowSize() > 1022 ? 3 : 4;

	const dispatch = useDispatch();

	const [popularityFilter, setPopularityFilter] = useState(true);
	const [priceFilter, setPriceFilter] = useState(false);
	const [lastArrValue, setLastArrValue] = useState(arrLength);

	const cardService = new CardServiсe;

	const cardsPriceFiltered = cards.filter((item) => item.type === 'main').sort(function (a, b) {
		if (b.price > a.price) {
			return -1;
		} if (b.price < a.price) {
			return 1;
		}

		return 0;
	})

	const cardsFinalFilter = priceFilter ? cardsPriceFiltered : cards.filter((item) => item.type === 'main');

	const cardsArr = cardsFinalFilter.slice(0, lastArrValue).map((item) => {
		let foundItem = cart.find((found) => found.id === item.id);

		if (foundItem) {
			return (
				<Card
					key={foundItem.id}
					card={foundItem} />
			)
		}

		return (
			<Card
				key={item.id}
				card={item} />
		)
	})

	const onLastArrValueInc = () => {
		return setLastArrValue(lastArrValue + incValue)
	}

	useEffect(() => {
		cardService.getCards()
			.then((data) => {
				dispatch(initialyzeCards(data))
			})
	}, []);

	useEffect(() => {
		setLastArrValue(arrLength)
	}, [arrLength]);

	return (
		<div className={styles.content} id={"catalog"}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<h2 className={styles.title}>Каталог аренды</h2>
					<div className={styles.tags}>
						<button className={cn(styles.tag, {
							[styles.tab_active]: popularityFilter
						})}
							onClick={() => {
								setPopularityFilter(true)
								setPriceFilter(false)
							}}>По популярности</button>
						<button className={cn(styles.tag, {
							[styles.tab_active]: priceFilter
						})}
							onClick={() => {
								setPopularityFilter(false)
								setPriceFilter(true)
							}}>По возрастанию цены</button>
					</div>
					<div className={styles.cards}>
						{cardsArr}
					</div>

					{lastArrValue < cardsFinalFilter.length && (
						<ShowMore
							onLastArrValueInc={onLastArrValueInc} />
					)}

				</div>
			</div>
		</div>
	);
}

export default Catalog;