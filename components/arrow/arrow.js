import styles from './arrow.module.css';
import cn from 'classnames';

const Arrow = ({ arrow = 'down', color = 'black' }) => {

	const setColor = color == 'black' ? '#001122' : '#FFFFFF';

	return (
		<div className={cn(styles.arrow, {
			[styles.up]: arrow == 'up'
		})}>
			<svg width="10" height="5" viewBox="0 0 10 5" xmlns="http://www.w3.org/2000/svg">
				<path d="M0 0L5 5L10 0H0Z" fill={setColor} />
			</svg>
		</div>
	)
}

export default Arrow;