
import styles from './catalog-tabs.module.css';

const CatalogTabItem = ({ isSelected, children }) => {

	if (isSelected) {
		return (
			<div className={styles.tab_inner}>
				{children}
			</div>
		)
	}

	return null;
}

export default CatalogTabItem;