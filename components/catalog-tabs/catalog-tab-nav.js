
import cn from 'classnames';
import styles from './catalog-tabs.module.css';

const CatalogTabNav = ({ selected, initSetSelected, children, tabs }) => {

	return (
		<div className={styles.tab_container}>
			<div className={styles.nav}>
				{
					tabs.map(tab => {
						const active = (tab === selected ? 'active' : '');

						return (
							<div className={styles.nav_tab} key={tab}>
								<a className={cn(styles.nav_link, {
									[styles.active]: active == 'active'
								})}
									onClick={() => initSetSelected(tab)}>
									{tab}
								</a>
							</div>
						)
					})
				}
			</div>

			{children}
		</div>
	)
}

export default CatalogTabNav;