import React, { useState } from 'react';
import styles from './header.module.css';
import Link from 'next/link';
import CartIcon from '../cart-icon/cart-icon';
import MenuPopup from '../menu-popup/menu-popup';
import LikeIcon from '../like-icon/like-icon';

const Header = () => {

	const [menuPopup, setMenuPopup] = useState(false);

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<nav className={styles.nav}>
						<Link href="/" passHref><a href="passed" className={styles.logo}>iTouch</a></Link>
						<div className={styles.menu}>
							<Link href={{ pathname: '/', hash: "#dev" }} passHref><a href="passed" className={styles.menuitem}>Разработка ПО</a></Link>
							<Link href={{ pathname: '/', hash: "#catalog" }} passHref><a href="passed" className={styles.menuitem}>Каталог</a></Link>
							<Link href={{ pathname: '/', hash: "#bonuses" }} passHref><a href="passed" className={styles.menuitem}>Бонусы</a></Link>
							<Link href={{ pathname: '/', hash: "#gallery" }} passHref><a href="passed" className={styles.menuitem}>Галерея</a></Link>
						</div>
						<p className={styles.whatsapp}><img src='/images/whatsapp.webp' alt="whatsapp" />+7 495 984 46 04</p>
						<span className={styles.icons}>
							<LikeIcon />
							<CartIcon />
						</span>
					</nav>
					<nav className={styles.small_menu}>
						<button className={styles.popup_menu}
							onClick={() => setMenuPopup(true)}>
							<img src='/images/menu-black.svg' alt="Menu" />
						</button>
						<p className={styles.small_intro_logo}>iTouch</p>
						<div className={styles.small_menu_icons}>
							<img className={styles.small_whatsapp} src='/images/whatsapp.webp' alt="whatsapp" />
							<LikeIcon />
							<CartIcon />
						</div>
					</nav>

				</div>
			</div>

			{menuPopup && (<MenuPopup setMenuPopup={setMenuPopup} />)}

		</div>

	);
}

export default Header;