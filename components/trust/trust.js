import React, { Component } from 'react';
import styles from './trust.module.css';

import { Swiper, SwiperSlide } from 'swiper/react';

import SwiperCore, {
	Navigation,
	Autoplay
} from 'swiper';

SwiperCore.use([Navigation, Autoplay]);


export default class Trust extends Component {

	render() {

		return (
			<div className={styles.trust} id={"section-trust"}>
				<h2>Нам доверяют</h2>
				<div className={styles.trust__inner}>
					<Swiper
						modules={[Autoplay]}
						autoplay={{
							"delay": 2500,
							"disableOnInteraction": false
						}}
						slidesPerView={4}
						loop={true}
						spaceBetween={50}

						breakpoints={{
							"0": {
								"slidesPerView": 1,
								"centeredSlides": true
							},
							"424": {
								"slidesPerView": 1.5
							},
							"640": {
								"slidesPerView": 2
							},
							"1150": {
								"slidesPerView": 3
							},
							"1400": {
								"slidesPerView": 4
							}
						}}
					>

						<SwiperSlide className={styles.trust__slide}>
							<img src='/images/stada-logo.webp' alt="Stada" />
						</SwiperSlide>

						<SwiperSlide className={styles.trust__slide}>
							<img src='/images/mercedes-logo.webp' alt="Mercedes" />
						</SwiperSlide>

						<SwiperSlide className={styles.trust__slide}>
							<img src='/images/megafon-logo.webp' alt="Megafon" />
						</SwiperSlide>

						<SwiperSlide className={styles.trust__slide}>
							<img src='/images/allegran-logo.webp' alt="Allegran" />
						</SwiperSlide>


					</Swiper>
				</div>
			</div>
		);
	}
}