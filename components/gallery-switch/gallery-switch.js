import useWindowSize from '../../hooks/useWindowSize';
import Gallery from '../gallery/gallery';
import GalleryMobile from '../gallery-mobile';

const GallerySwitch = () => {
	return (
		<div id={"gallery"}>
			{useWindowSize() > 767 && <Gallery />}
			{useWindowSize() < 767 && <GalleryMobile />}
		</div>
	);
}

export default GallerySwitch;