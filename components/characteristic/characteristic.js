import React from 'react';
import Link from 'next/link';
import styles from './characteristic.module.css';

const Characteristic = ({ id, imgUrl, characteristics, setCharPopup }) => {

	let i = 0;

	const chars = characteristics.map((item) => {
		return (
			<p key={i++}>{item.info}</p>
		)
	})

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<button onClick={() => setCharPopup(false)} className={styles.close}>
						<img src="images/arrow-left.svg" />Вернуться в каталог
					</button>
					<div className={styles.card}>
						<div className={styles.picture}>
							<picture className={styles.picture1}>
								<source srcSet={imgUrl} type="image/webp" />
								<img src={imgUrl} alt="product" className={styles.img} />
							</picture>
						</div>
						<div className={styles.text}>
							<h3>Характеристики</h3>
							{chars}
						</div>
					</div>

				</div>
			</div>
		</div>
	);
}

export default Characteristic;