import React, { useState } from 'react';
import styles from './bonuses.module.css';
import { IMaskInput } from 'react-imask';
import cn from 'classnames';
import Arrow from '../arrow/arrow';
import AcceptPopup2 from '../accept-popup-2';

const Bonuses = () => {

	const [popupStatus, setPopupStatus] = useState(false);
	const [selectorValue, setSelectorValue] = useState('Позвонить по телефону');
	const [acceptPopupStatus, setAcceptPopupStatus] = useState(false);
	const onHandleInputChange = (e) => {
		localStorage.setItem('phoneNumber', e.target.value);
	}

	return (
		<div className={styles.content} id={"bonuses"}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<div className={styles.left}>
						<img className={styles.iphone} src="../../images/iphone.webp" alt="iphone cover" />
						<img className={styles.iphone_small} src="../../images/iphone-small.webp" alt="iphone cover" />
						<form className={styles.form} name="register"
							onSubmit={(event) => {
								event.preventDefault()
								setAcceptPopupStatus(true)
							}}>
							<IMaskInput
								mask={'+{7}(000)000-00-00'}
								placeholder="+ 7 (___)-___-__-__"
								pattern="^(\+7|8)((\(\d{3}\)\s?\d{3}-\d{2}-\d{2})|(\s\d{3}-\d{3}-\d{2}-\d{2})|(\d{10}))$"
								minLength="11"
								maxLength="18"
								required
								onChange={onHandleInputChange}
							/>
							<div className={styles.selector}>
								<button className={cn(styles.selector_default, {
									[styles.opened]: popupStatus == true
								})} type='button'
									onClick={() => {
										setPopupStatus(!popupStatus)
									}}>
									{selectorValue}
									<Arrow
										arrow={popupStatus ? 'up' : 'down'}
										color={popupStatus ? 'white' : 'black'} />
								</button>

								{popupStatus && (
									<div className={styles.selector_list}>
										<button className={styles.selector_item}
											onClick={() => {
												setSelectorValue('Позвонить по телефону')
												setPopupStatus(false)
											}}>
											Позвонить по телефону
										</button>
										<button className={styles.selector_item}
											onClick={() => {
												setSelectorValue('Написать в WhatsApp')
												setPopupStatus(false)
											}}>
											Написать в WhatsApp
										</button>
										<button className={styles.selector_item}
											onClick={() => {
												setSelectorValue('Написать в Telegram')
												setPopupStatus(false)
											}}>
											Написать в Telegram
										</button>
									</div>
								)}

							</div>
							<button className={styles.submit} type="submit">Получить консультацию</button>
						</form>
					</div>

					<div className={styles.right}>
						<div className={styles.text}>
							<h2 className={styles.heading}>Бонусы для клиентов!</h2>
							<div className={styles.text_item}><span className={styles.round}></span><p><span className={styles.blue}>Бесплатная доставка</span> в будние дни</p></div>
							<div className={styles.text_item}><span className={styles.round}></span><p>При заказе разработки ПО — аренда плазмы <span className={styles.blue}>в подарок</span></p></div>
							<div className={styles.text_item}><span className={styles.round}></span><p>При заказе от 2-х столов доставка <span className={styles.blue}>бесплатная</span></p></div>
							<div className={styles.text_item}><span className={styles.round}></span><p>Персональный менеджер на связи <span className={styles.blue}>24 часа в сутки</span></p></div>
						</div>
					</div>

				</div>
			</div>

			{acceptPopupStatus && <AcceptPopup2
				setAcceptPopupStatus={setAcceptPopupStatus} />}

		</div>
	);
}

export default Bonuses;