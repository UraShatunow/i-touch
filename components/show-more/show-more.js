import styles from './show-more.module.css';

const ShowMore = ({ onLastArrValueInc }) => {
	// const {onClick} = props;

	return (
		<button className={styles.show_more} onClick={onLastArrValueInc}>
			Показать ещё
			<img src="/images/arrow-small.svg" />
		</button>
	);
}

export default ShowMore;