/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import styles from './dev-popup.module.css';
import Link from 'next/link';
import cn from 'classnames';
import { IMaskInput } from 'react-imask';
import Arrow from '../arrow/arrow';

const DevPopup = ({ setDevPopupStatus, setAcceptPopupStatus, title }) => {

	const titleValue = title ? title : 'Заявка';
	const [popupStatus, setPopupStatus] = useState(false);
	const [selectorValue, setSelectorValue] = useState('Позвонить по телефону');
	const onHandleInputChange = (e) => {
		localStorage.setItem('phoneNumber', e.target.value);
	}

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<header className={styles.header}>
						<a className={styles.back} onClick={() => {
							setDevPopupStatus(false)
						}}>
							<img src="/images/arrow-back.svg" alt="back" />Вернуться
						</a>
						<a className={styles.back2} onClick={() => {
							setDevPopupStatus(false)
						}}>
							<img src="/images/arrow-back.svg" alt="back" />
						</a>
						<img className={styles.logo} src="/images/logo-white.svg" alt="logo" />
						<button className={styles.close} onClick={() => {
							setDevPopupStatus(false)
						}}>
							Закрыть<img src="/images/close.svg" />
						</button>
						<button className={styles.close2} onClick={() => {
							setDevPopupStatus(false)
						}}>
							<img src="/images/close.svg" />
						</button>
					</header>

					<section className={styles.section}>
						<div className={styles.section_inner}>
							<h2 className={styles.title}>{titleValue}</h2>
							<p className={styles.paragraph}>Выберите удобную форму для обращения.
								Наш менеджер свяжется с вами в ближайшее время и ответит на все вопросы.</p>
							<form className={styles.form} onSubmit={(event) => {
								event.preventDefault()
								setDevPopupStatus(false)
								setAcceptPopupStatus(true)
							}} >
								<IMaskInput
									mask={'+{7}(000)000-00-00'}
									placeholder="+ 7 (___)-___-__-__"
									pattern="^(\+7|8)((\(\d{3}\)\s?\d{3}-\d{2}-\d{2})|(\s\d{3}-\d{3}-\d{2}-\d{2})|(\d{10}))$"
									minLength="11"
									maxLength="18"
									required
									className={styles.phone}
									onChange={onHandleInputChange}
								/>
								<div className={styles.selector}>
									<button className={cn(styles.selector_default, {
										[styles.opened]: popupStatus == true
									})} type='button'
										onClick={() => {
											setPopupStatus(!popupStatus)
										}}>
										{selectorValue}
										<Arrow
											arrow={popupStatus ? 'up' : 'down'}
											color={popupStatus ? 'white' : 'black'} />
									</button>

									{popupStatus && (
										<div className={styles.selector_list}>
											<button className={styles.selector_item}
												onClick={() => {
													setSelectorValue('Позвонить по телефону')
													setPopupStatus(false)
												}}>
												Позвонить по телефону
											</button>
											<button className={styles.selector_item}
												onClick={() => {
													setSelectorValue('Написать в WhatsApp')
													setPopupStatus(false)
												}}>
												Написать в WhatsApp
											</button>
											<button className={styles.selector_item}
												onClick={() => {
													setSelectorValue('Написать в Telegram')
													setPopupStatus(false)
												}}>
												Написать в Telegram
											</button>
										</div>
									)}

								</div>
								<input className={styles.submit} type="submit" value='Отправить заявку' />
								<div className={styles.checkbox_area}>
									<input className={styles.checkbox} id="agree" name="agree" type="checkbox" required />
									<label className={styles.fake_checkbox} htmlFor="agree"><p>Я даю согласие на<span>обработку персональных данных</span></p></label>
								</div>
							</form>
						</div>
					</section>

				</div>
			</div>
		</div>
	);
}

export default DevPopup;