import React from 'react';
import Link from 'next/link';
import styles from './like-icon.module.css';
import { useSelector } from 'react-redux';

const LikeIcon = () => {

	const cards = useSelector(state => state.favouriteState.favourites);

	let count = 0;

	cards.forEach(() => {
		return count++;
	})

	return (
		<Link href="/favourites" passHref>
			<a href="passed" className={styles.content}>
				<img src="/images/like.svg" alt="cart" className={styles.img} />
				{count > 0 && (<span className={styles.count}>{count}</span>)}
			</a>
		</Link>
	);
}

export default LikeIcon;