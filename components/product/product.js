import React, { useState, useEffect } from 'react';
import styles from './product.module.css';
import cn from 'classnames';
import Characteristic from '../characteristic/characteristic';
import { useDispatch } from 'react-redux';
import { incItemCount, delItem, updateItem } from '../../redux/actions/cartActions';
import useWindowSize from '../../hooks/useWindowSize';
import Arrow from '../arrow/arrow';
import HeartIcon from '../heart-icon';

const Product = ({ cartItem }) => {

	const dispatch = useDispatch();
	const screenWidth = useWindowSize()
	const { count, id, imgUrl, imgUrlj, title, price, day, desc, noDays, characteristics, tag } = cartItem;

	const checkSelector = (day) => {
		switch (day) {
			case 1:
				return '1 день'
			case 2:
				return '2 дня'
			case 3:
				return '3 дня'
			case 4:
				return '4 дня'
			case 5:
				return '5 дней'
		}
	}

	const [daysButtonStatus, setDaysButtonStatus] = useState(day);
	const [charPopup, setCharPopup] = useState(false);
	const [selectorValue, setSelectorValue] = useState(checkSelector(daysButtonStatus));
	const [selectorIsOpen, setSelectorIsOpen] = useState(false);

	const countThePrice = (daysButtonStatus) => {
		switch (daysButtonStatus) {
			case 1:
				return price

			case 2:
				return price + (price * 0.5)

			case 3:
				return price + (price * 0.5) + (price * 0.4)

			case 4:
				return price + (price * 0.5) + (price * 0.4) + (price * 0.35)

			case 5:
				return price + (price * 0.5) + (price * 0.4) + (price * 0.35) + (price * 0.3)

			default:
				return price * 2;
		}
	}

	const x = count === 0 ? 1 : count;
	const priceValue = countThePrice(daysButtonStatus);
	const floorPrice = (Math.floor(priceValue / 100) * 100) * x;
	const priceRu = floorPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').concat(' ₽');

	const checkDesc = desc ? true : false;
	const checkDaysVisible = noDays | screenWidth < 768 ? true : false;
	const checkScreenWidth = screenWidth < 768 ? true : false;
	const checkFinal = checkDesc | checkScreenWidth ? true : false;
	const checkfinal2 = checkFinal ? true : false;

	useEffect(() => {
		dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, count, day, total: floorPrice, desc, noDays, characteristics }))
	}, [floorPrice])

	return (
		<div className={styles.product}>

			<div className={styles.img}>
				<picture className={styles.picture}>
					<source srcSet={imgUrlj} type="image/webp" />
					<img className={styles.img_fill} src={imgUrl} alt="MDN" />
				</picture>
			</div>

			<div className={styles.product_inner}>

				<div className={styles.heading}>
					<h4>{title}</h4>
					<button onClick={() => {
						dispatch(delItem({
							id
						}))
					}}>
						<img src='/images/trash.svg' alt='trash'
						/>
					</button>

					<div className={styles.like}>
						<HeartIcon card={cartItem} />
					</div>

				</div>
				<div className={styles.info}>



					{checkfinal2 || (<a className={styles.char}
						onClick={() => setCharPopup(true)}>Характеристики</a>)}

					{(checkFinal && (screenWidth > 767)) && (<p className={styles.desc}>{desc}</p>)}

				</div>
				{checkDaysVisible || (
					<div className={styles.days}>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 1,
						})} onClick={(() => {
							setDaysButtonStatus(1)
							dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
						})}>1 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 2,
						})} onClick={(() => {
							setDaysButtonStatus(2)
							dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 2, noDays, desc, characteristics }))
						})}>2 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 3,
						})} onClick={(() => {
							setDaysButtonStatus(3)
							dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 3, noDays, desc, characteristics }))
						})}>3 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 4,
						})} onClick={(() => {
							setDaysButtonStatus(4)
							dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 4, noDays, desc, characteristics }))
						})}>4 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 5,
						})} onClick={() => {
							setDaysButtonStatus(5)
							dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 5, noDays, desc, characteristics }))
						}} >5 дн</button>
					</div>
				)}
				{(checkDaysVisible && (screenWidth < 768)) && (
					<div className={styles.selector}>
						<button className={styles.selector_default}
							onClick={() => setSelectorIsOpen(!selectorIsOpen)}>
							{selectorValue}
							<Arrow
								arrow={selectorIsOpen ? 'up' : 'down'} />
						</button>
						{selectorIsOpen && (
							<div className={styles.selector_list}>
								<button onClick={(() => {
									setSelectorValue('1 день')
									setDaysButtonStatus(1)
									dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
									setSelectorIsOpen(false)
								})}>1 день</button>
								<button onClick={(() => {
									setSelectorValue('2 дня')
									setDaysButtonStatus(2)
									dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
									setSelectorIsOpen(false)
								})}>2 дня</button>
								<button onClick={(() => {
									setSelectorValue('3 дня')
									setDaysButtonStatus(3)
									dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
									setSelectorIsOpen(false)
								})}>3 дня</button>
								<button onClick={(() => {
									setSelectorValue('4 дня')
									setDaysButtonStatus(4)
									dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
									setSelectorIsOpen(false)
								})}>4 дня</button>
								<button onClick={(() => {
									setSelectorValue('5 дней')
									setDaysButtonStatus(5)
									dispatch(updateItem({ id, imgUrl, imgUrlj, title, price, total: floorPrice, count, day: 1, noDays, desc, characteristics }))
									setSelectorIsOpen(false)
								})}>5 дней</button>
							</div>
						)}
					</div>
				)}
				<div className={cn(styles.cost, {
					[styles.noDays]: noDays == true
				})}>
					<div className={styles.sum}>
						{priceRu}
					</div>
					<div className={styles.cart_opened}>

						{count > 1 && (
							<button onClick={() => {
								{
									count == 1 ? dispatch(delItem({ id })) : dispatch(incItemCount({
										id,
										imgUrl,
										imgUrlj,
										title,
										price,
										total: floorPrice,
										count: count - 1,
										day: daysButtonStatus,
										noDays,
										desc,
										tag,
										characteristics
									}))
								}
							}}>
								<img src="/images/minus.svg" alt="minus" />
							</button>
						)}

						{count == 1 && (
							<button className={styles.disabled}>
								<img src="/images/minus-disabled.svg" alt="minus" />
							</button>
						)}

						<p>{count} шт</p>
						<button onClick={() => {
							dispatch(incItemCount({
								id,
								imgUrl,
								imgUrlj,
								title,
								price,
								count: count + 1,
								day: daysButtonStatus,
								total: floorPrice,
								noDays,
								desc,
								characteristics
							}))
						}}><img src="/images/plus.svg" alt="plus" /></button>
					</div>
				</div>

			</div>

			{charPopup && <Characteristic
				id={id}
				imgUrl={imgUrlj}
				characteristics={characteristics}
				setCharPopup={setCharPopup} />}

		</div>
	);
}


export default (Product);