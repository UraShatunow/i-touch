import React, { Component } from 'react';
import styles from './footer.module.css';
import Link from 'next/link';

export default class Footer extends Component {

	render() {

		return (
			<footer className={styles.content}>
				<div className={styles.container}>
					<div className={styles.inner}>
						<div className={styles.copyright}>
							<p>Copyright 2021 iTouch</p>
							<p>Все права защищены</p>
							<Link href="/confidential" passHref><a href="passed">Политика конфиденциальности</a></Link>
							<Link href="/news" passHref><a href="passed">Новости</a></Link>
						</div>
						<div className={styles.menu}>
							<Link href={{ pathname: '/', hash: "#dev" }} passHref><a href="passed">Разработка ПО</a></Link>
							<Link href={{ pathname: '/', hash: "#catalog" }} passHref><a href="passed">Каталог</a></Link>
							<Link href={{ pathname: '/', hash: "#bonuses" }} passHref><a href="passed">Бонусы</a></Link>
							<Link href={{ pathname: '/', hash: "#gallery" }} passHref><a href="passed">Галерея</a></Link>
						</div>
						<div className={styles.socials}>
							<Link href="/" passHref><a href="passed"><img src="/images/vk.webp" /></a></Link>
							<Link href="/" passHref><a href="passed"><img src="/images/whatsapp-40.webp" /></a></Link>
							<Link href="/" passHref><a href="passed"><img src="/images/fb.webp" /></a></Link>
							<Link href="/" passHref><a href="passed"><img src="/images/tg.webp" /></a></Link>
							<Link href="/" passHref><a href="passed"><img src="/images/insta.webp" /></a></Link>
						</div>
						<div className={styles.contacts}>
							<p>Контакты</p>
							<p>info@art-active.ru</p>
							<p>Москва, Егорьевский проезд, д.2а, стр.8</p>
							<Link href="/" passHref><a href="passed">+7 495 984 46 04</a></Link>
						</div>
					</div>
				</div>
			</footer>
		);
	}
}