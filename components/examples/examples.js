import React, { Component } from 'react';
import styles from './examples.module.css';
import Link from 'next/link';

import { Swiper, SwiperSlide } from 'swiper/react';

import SwiperCore, {
	Pagination
} from 'swiper';

SwiperCore.use([Pagination]);

export default class Examples extends Component {

	render() {

		return (
			<div className={styles.content}>
				<div className={styles.container}>
					<div className={styles.inner}>

						<div className={styles.item}>
							<div className={styles.slider}>
								<Swiper
									modules={[Pagination]}
									slidesPerView={1}
									pagination={{
										"clickable": true
									}}
									className="swiper_examples"
								>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-1.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-2.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-3.webp' alt="example" />
										</div>
									</SwiperSlide>

								</Swiper>
							</div>
							<div className={styles.text}>
								<span className={styles.number}>01</span>
								<h2>Решим простые и сложные креативные задачи по вашему ТЗ</h2>
								<p>Реализуем логику приложения</p>
							</div>
						</div>
						
						<div className={styles.link}>
							<Link href="/" passHref>
								<a href="passed" className={styles.link_item}><img src='/images/arrow.webp' alt="link" /></a>
							</Link>
						</div>

						<div className={styles.item}>
							<div className={styles.slider}>
								<Swiper
									modules={[Pagination]}
									slidesPerView={1}
									pagination={{
										"clickable": true
									}}
									className="swiper_examples"
								>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-2.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-3.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-4.webp' alt="example" />
										</div>
									</SwiperSlide>

								</Swiper>
							</div>
							<div className={styles.text}>
								<span className={styles.number}>02</span>
								<h2>Дизайнеры создадут понятный интерфейс</h2>
								<p>Проектируем экраны, инфографику, таблицы, создаём анимацию объектов</p>
							</div>
						</div>
						
						<div className={styles.link}>
							<Link href="/" passHref>
								<a href="passed" className={styles.link_item}><img src='/images/arrow.webp' alt="link" /></a>
							</Link>
						</div>

						<div className={styles.item}>
							<div className={styles.slider}>
								<Swiper
									modules={[Pagination]}
									slidesPerView={1}
									pagination={{
										"clickable": true
									}}
									className="swiper_examples"
								>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-3.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-4.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-1.webp' alt="example" />
										</div>
									</SwiperSlide>

								</Swiper>
							</div>
							<div className={styles.text}>
								<span className={styles.number}>03</span>
								<h2>Напишем ПО для квизов, рекламы, навигации, для участия в акциях, играх</h2>
								<p>Используем современный инструментарий для презентаций</p>
							</div>
						</div>

						<div className={styles.link}>
							<Link href="/" passHref>
								<a href="passed" className={styles.link_item}><img src='/images/arrow.webp' alt="link" /></a>
							</Link>
						</div>
						
						<div className={styles.item}>
							<div className={styles.slider}>
								<Swiper
									modules={[Pagination]}
									slidesPerView={1}
									pagination={{
										"clickable": true
									}}
									className="swiper_examples"
								>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-4.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-1.webp' alt="example" />
										</div>
									</SwiperSlide>

									<SwiperSlide>
										<div className={styles.wrapper}>
											<img src='/images/examples-2.webp' alt="example" />
										</div>
									</SwiperSlide>

								</Swiper>
							</div>
							<div className={styles.text}>
								<span className={styles.number}>04</span>
								<h2>Специалисты по брендингу
									декорируют оборудование</h2>
								<p>Оклеивсаем корпусы плёнкой ORACAL или ORAJET
									с полноцветным нанесением брендига</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
}