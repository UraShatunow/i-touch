/* eslint-disable @next/next/no-img-element */
import React from 'react';
import styles from './favourite.module.css';
import { useSelector } from 'react-redux';
import Card from '../card/card';
import Link from 'next/dist/client/link';

const Favourite = () => {

	const cards2 = useSelector(state => state.catalogState.cards);
	const cards = useSelector(state => state.favouriteState.favourites);
	const cart = useSelector(state => state.catalogState.cart);

	const cardsArr = cards.map((item) => {
		let foundItem = cart.find((found) => found.id === item.id);
		let foundItem2 = cards2.find((found) => found.id === item.id);

		if (foundItem) {
			return (
				<Card
					key={foundItem.id}
					card={foundItem} />
			)
		}

		if (foundItem2) {
			return (
				<Card
					key={foundItem2.id}
					card={foundItem2} />
			)
		}

		return (
			<Card
				key={item.id}
				card={item} />
		)
	})

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<Link href="/" passHref>
						<a href="passed" className={styles.home_link}>
							<img src='/images/arrow-left.svg' alt='arrow' />
							Вернуться на главную
						</a>
					</Link>

					<section className={styles.section}>

						<div className={styles.cart}>
							<h2>Избранное</h2>

							{cards.length < 1 && (
								<div className={styles.empty}>
									<h3>Здесь пока ничего нет :(</h3>
									<p>Вам наверняка понравится что-то из нашего каталога</p>
									<Link href={{ pathname: '/', hash: "#catalog" }} passHref>
										<a href="passed" className={styles.to_catalog}>
											Перейти в каталог
										</a>
									</Link>
								</div>
							)}

							<div className={styles.cart_inner}>
								{cardsArr}
							</div>
						</div>


						<div className={styles.bonuses}>
							<h3>Бонусы для клиентов!</h3>
							<div className={styles.text}>
								<div className={styles.group}>
									<div className={styles.bonus}><span className={styles.round}></span><p><span className={styles.blue}>Бесплатная доставка</span> в будние дни</p></div>
									<div className={styles.bonus}><span className={styles.round}></span><p>При заказе разработки ПО — аренда плазмы <span className={styles.blue}>в подарок</span></p></div>
								</div>
								<div className={styles.group}>
									<div className={styles.bonus}><span className={styles.round}></span><p>При заказе от 2-х столов доставка <span className={styles.blue}>бесплатная</span></p></div>
									<div className={styles.bonus}><span className={styles.round}></span><p>Персональный менеджер на связи <span className={styles.blue}>24 часа в сутки</span></p></div>
								</div>
							</div>
						</div>

					</section>

				</div>
			</div>
		</div>
	);
}

export default Favourite;