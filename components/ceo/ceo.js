import React from 'react';
import styles from './ceo.module.css';

const Ceo = () => {

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<div className={styles.card}>
						<div className={styles.text}>
							<h3 className={styles.heading}>Сенсорный стол в аренду</h3>
							<p className={styles.paragraph}>Интерактивный стол — это современный и высокотехнологичный вариант предоставления информации гостям мероприятий. Благодаря устройству с большим сенсорным экраном каждый посетитель сможет получить ответы на все интересующие его вопросы без участия консультирующего работника. Интерактивный стол может быть использован для показа презентаций, воспроизведения видео- и аудиозаписей или в развлекательных целях.</p>
						</div>
						<div className={styles.pic}>
							<picture className={styles.pciture}>
								<source srcSet='/images/ceo-1.jpg' type="image/webp" />
								<img className={styles.img} src='/images/ceo-1.webp' alt='product' />
							</picture>
						</div>
					</div>

					<div className={styles.card}>
						<div className={styles.text}>
							<h3 className={styles.heading}>Интерактивный стол на выставку</h3>
							<p className={styles.paragraph}>С особым успехом интерактивные столы используются на выставках. Посетители, которым комфортнее изучать экспонаты самостоятельно, без сопровождения экскурсовода, смогут ознакомиться с подробным и интересным описанием представленных позиций. Устройство интуитивно понятно, и разобраться в работе с сенсорным экраном не составит труда даже пожилым гостям. Такой вариант представления информации покажет, что организаторы встречи идут в ногу со временем.</p>
						</div>
						<div className={styles.pic}>
							<picture className={styles.pciture}>
								<source srcSet='/images/ceo-2.jpg' type="image/webp" />
								<img className={styles.img} src='/images/ceo-2.webp' alt='product' />
							</picture>
						</div>
					</div>

					<div className={styles.card}>
						<div className={styles.text}>
							<h3 className={styles.heading}>Аренда тач стола</h3>
							<p className={styles.paragraph}>Хотите заказать интерактивный стол в аренду с высокой производительностью и оптимальной диагональю на мероприятие? Мы предоставим в аренду качественное оборудование по выгодной цене на нужный Вам срок. Организуем доставку техники по указанному адресу и поможем с её установкой. По желанию клиента также возможно индивидуальное брендирование сенсорного стола и предоставление услуг инструктора или аниматора. Поможем предоставить гостям максимум удобств!</p>
						</div>
						<div className={styles.pic}>
							<picture className={styles.pciture}>
								<source srcSet='/images/ceo-3.jpg' type="image/webp" />
								<img className={styles.img} src='/images/ceo-3.webp' alt='product' />
							</picture>
						</div>
					</div>

				</div>
			</div>
		</div>
	);
}

export default Ceo;