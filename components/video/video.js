import React, { useEffect, useState } from 'react';
import styles from './video.module.css';
import useWindowSize from '../../hooks/useWindowSize';
import DevPopup from '../dev-popup';
import AcceptPopup from '../accept-popup';

const Video = () => {

	const [isVideoPopupOpened, setIsVideoPopupOpened] = useState(false);
	const [devPopupStatus, setDevPopupStatus] = useState(false);
	const [acceptPopupStatus, setAcceptPopupStatus] = useState(false);

	return (
		<div className={styles.video} id={"dev"}>
			<div className={styles.video__container}>
				<div className={styles.video__inner}>

					{useWindowSize() > 769 && (
						<div className={styles.desktop}>
							<div className={styles.text}>
								<p className={styles.paragraph}>Мы разработаем для вас программное обеспечение</p>
								<button className={styles.button} onClick={() => {
									setDevPopupStatus(true)
								}}>Заказать разработку ПО</button>
							</div>
							<video className={styles.video__video} autoPlay loop muted>
								<source src="/videos/video-mp4.mp4" type="video/mp4" />
								<source src="/videos/video-ogv.mp4" type="video/ogv" />
								<source src="/videos/video-webm.mp4" type="video/webm" />
							</video>
						</div>
					)}

					{useWindowSize() < 770 && (
						<div className={styles.mobile}>
							<div className={styles.text}>
								<p className={styles.paragraph}>Мы разработаем для вас<br /> программное обеспечение</p>
								<button className={styles.youtube}
									onClick={() => setIsVideoPopupOpened(true)}>
									<img src='/images/youtube.svg' alt="youtube" />
								</button>
								<button className={styles.button} onClick={() => {
									setDevPopupStatus(true)
								}}>Заказать разработку ПО</button>
							</div>
							<img className={styles.background} src='/images/video-bg.jpg' alt="background" />
						</div>
					)}


					{isVideoPopupOpened &&
						(<div className={styles.popup}>
							<div className={styles.popup_inner}>
								<iframe className={styles.frame} id="ytplayer" type="text/html"
									src="https://www.youtube.com/embed/LFhmDGN01aA"
									frameBorder="0" />
								<button
									onClick={() => setIsVideoPopupOpened(false)}>
									Закрыть
									<img src='images/close.webp' />
								</button>
							</div>
						</div>)}

				</div>
			</div>

			{devPopupStatus && <DevPopup
				setDevPopupStatus={setDevPopupStatus}
				setAcceptPopupStatus={setAcceptPopupStatus}
				title={'Заявка на разработку ПО'} />}
			{acceptPopupStatus && <AcceptPopup
				setDevPopupStatus={setDevPopupStatus}
				setAcceptPopupStatus={setAcceptPopupStatus} />}

		</div>
	);
}

export default Video;