import React, { useState } from 'react';
import styles from './menu-popup.module.css';
import Link from 'next/link';

const MenuPopup = ({ setMenuPopup }) => {



	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<button className={styles.close}
						onClick={() => setMenuPopup(false)}>
						<img src='/images/close-black.svg' alt='close' />
					</button>
					<Link href={{ pathname: '/', hash: "#dev" }} passHref>
						<a href="passed" className={styles.link} onClick={() => setMenuPopup(false)}>
							Разработка ПО
						</a>
					</Link>
					<Link href={{ pathname: '/', hash: "#catalog" }} passHref>
						<a href="passed" className={styles.link} onClick={() => setMenuPopup(false)}>
							Каталог аренды
						</a>
					</Link>
					<Link href="/" passHref>
						<a href="passed" className={styles.link} onClick={() => setMenuPopup(false)}>
							<img src='/images/phone.svg' alt='telegram' />+7 495 984 46 04
						</a>
					</Link>
					<Link href="/" passHref>
						<a href="passed" className={styles.link} onClick={() => setMenuPopup(false)}>
							<img src='/images/whatsapp.png' alt='telegram' />Написать в WatsApp
						</a>
					</Link>
					<Link href="/" passHref>
						<a href="passed" className={styles.link} onClick={() => setMenuPopup(false)}>
							<img src='/images/tg.png' alt='telegram' />Написать в Telegram
						</a>
					</Link>

				</div>
			</div>
		</div>
	)
}

export default MenuPopup;