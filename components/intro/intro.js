import React, { useState } from 'react';
import Link from 'next/link';
import styles from './intro.module.css';
import CartIcon from '../cart-icon';
import MenuPopup from '../menu-popup/menu-popup';
import useScroll from '../../hooks/useScroll';
import LikeIcon from '../like-icon/like-icon';

const Intro = () => {

	const [menuPopup, setMenuPopup] = useState(false);
	const scrollY = useScroll().scrollY;

	return (
		<div className={styles.intro}>
			<div className={styles.intro__container}>

				{scrollY > 50 && (
					<div className={styles.nav_fixed_container}>
						<nav className={styles.intro__nav_fixed}>
							<p className={styles.intro__logo}>iTouch</p>
							<div className={styles.intro__menu}>
								<Link href={{ pathname: '/', hash: "#dev" }} passHref><a href="passed" className={styles.intro__menuitem}>Разработка ПО</a></Link>
								<Link href={{ pathname: '/', hash: "#catalog" }} passHref><a href="passed" className={styles.intro__menuitem}>Каталог</a></Link>
								<Link href={{ pathname: '/', hash: "#bonuses" }} passHref><a href="passed" className={styles.intro__menuitem}>Бонусы</a></Link>
								<Link href={{ pathname: '/', hash: "#gallery" }} passHref><a href="passed" className={styles.intro__menuitem}>Галерея</a></Link>
							</div>
							<p className={styles.intro__whatsapp}><img src='/images/whatsapp.webp' alt="whatsapp" />+7 495 984 46 04</p>
							<span className={styles.intro__icons}>
								<LikeIcon />
								<CartIcon />
							</span>
						</nav>
					</div>
				)}

				<nav className={styles.intro__nav}>
					<p className={styles.intro__logo}>iTouch</p>
					<div className={styles.intro__menu}>
						<Link href={{ pathname: '/', hash: "#dev" }} passHref><a href="passed" className={styles.intro__menuitem}>Разработка ПО</a></Link>
						<Link href={{ pathname: '/', hash: "#catalog" }} passHref><a href="passed" className={styles.intro__menuitem}>Каталог</a></Link>
						<Link href={{ pathname: '/', hash: "#bonuses" }} passHref><a href="passed" className={styles.intro__menuitem}>Бонусы</a></Link>
						<Link href={{ pathname: '/', hash: "#gallery" }} passHref><a href="passed" className={styles.intro__menuitem}>Галерея</a></Link>
					</div>
					<p className={styles.intro__whatsapp}><img src='/images/whatsapp.webp' alt="whatsapp" />+7 495 984 46 04</p>
					<span className={styles.intro__icons}>
						<LikeIcon />
						<CartIcon />
					</span>
				</nav>

				{scrollY > 55 && (
					<div className={styles.nav_small_fixed_container}>
						<nav className={styles.intro_small_menu_fixed}>
							<button className={styles.popup_menu}
								onClick={() => setMenuPopup(true)}>
								<img src='/images/menu-small-icon.svg' alt="Menu" />
							</button>
							<p className={styles.small_intro_logo}>iTouch</p>
							<div className={styles.small_menu_icons}>
								<img className={styles.small_whatsapp} src='/images/whatsapp.webp' alt="whatsapp" />
								<LikeIcon />
								<CartIcon />
							</div>
						</nav>
					</div>
				)}

				<nav className={styles.intro_small_menu}>
					<button className={styles.popup_menu}
						onClick={() => setMenuPopup(true)}>
						<img src='/images/menu-small-icon.svg' alt="Menu" />
					</button>
					<p className={styles.small_intro_logo}>iTouch</p>
					<div className={styles.small_menu_icons}>
						<img className={styles.small_whatsapp} src='/images/whatsapp.webp' alt="whatsapp" />
						<LikeIcon />
						<CartIcon />
					</div>
				</nav>
				<div className={styles.intro__inner}>
					<h1 className={styles.intro__title}>Аренда интерактивного стола</h1>
					<p>Хотите значительно повысить эффективность на выставке?<br />
						Аренда тач-стола от 15 000 ₽. Разрабатываем программы
						по индивидуальному заказу</p>
					<Link href={{ pathname: '/', hash: "#catalog" }} passHref><a href="passed" className={styles.intro__button}>Перейти к каталогу</a></Link>
					<Link href={{ pathname: '/', hash: "#section-trust" }} passHref><a href="passed" className={styles.intro__arrow}></a></Link>
				</div>
			</div>

			{menuPopup && (<MenuPopup setMenuPopup={setMenuPopup} />)}

		</div>
	);
}

export default Intro;