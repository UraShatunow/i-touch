import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import styles from './heart-icon.module.css';
import { useDispatch, useSelector } from 'react-redux';
import { addFavouriteItem, deleteFavouriteItem } from '../../redux/actions/favouriteActions';

const HeartIcon = ({ card }) => {
	const { id, title, imgUrl, imgUrlj, price, count, day, characteristics, tag, desc, noDays } = card;

	const favouriteItems = useSelector(state => state.favouriteState.favourites);
	const [isLiked, setIsLiked] = useState(false)
	const dispatch = useDispatch();

	useEffect(() => {
		if (favouriteItems.find(item => item.id === id)) {
			setIsLiked(true);
		} else {
			setIsLiked(false);
		}
	}, [favouriteItems]);

	return (
		<>
			{isLiked || (<button className={styles.like} onClick={() => {
				setIsLiked(true)
				dispatch(addFavouriteItem({
					id,
					imgUrl,
					imgUrlj,
					title,
					price,
					count,
					noDays,
					desc,
					tag,
					characteristics
				}))
			}}></button>)}
			{isLiked && (<button className={styles.like_active} onClick={() => {
				setIsLiked(false)
				dispatch(deleteFavouriteItem({
					id,
					imgUrl,
					imgUrlj,
					title,
					price,
					count,
					noDays,
					desc,
					tag,
					characteristics
				}))
			}}></button>)}
		</>
	);
}

export default HeartIcon;