import styles from './gallery.module.css';
import cn from 'classnames';
import { useState } from 'react';

const Gallery = () => {

	const [imgUrl, setImgUrl] = useState(null);
	const [videoUrl, setVideoUrl] = useState(null);
	const [isPopupOpened, setIsPopupOpened] = useState(false);
	const [isVideoPopupOpened, setIsVideoPopupOpened] = useState(false);

	return (
		<div className={styles.content}>
			<div className={styles.container}>

				<h2>Галерея интерактива на мероприятиях</h2>

				<div className={styles.inner}>

					<figure className={cn(styles.image_1, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-1.webp'} type="image/webp" />
							<img src={'/images/gallery-1.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-1l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_2, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-2.webp'} type="image/webp" />
							<img src={'/images/gallery-2.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setVideoUrl('https://www.youtube.com/embed/wAzNN5wItB4')
								setIsVideoPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_3, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-3.webp'} type="image/webp" />
							<img src={'/images/gallery-3.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-3l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_4, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-4.webp'} type="image/webp" />
							<img src={'/images/gallery-4.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-4l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_5, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-5.webp'} type="image/webp" />
							<img src={'/images/gallery-5.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-5l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_6, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-6.webp'} type="image/webp" />
							<img src={'/images/gallery-6.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setVideoUrl('https://www.youtube.com/embed/zETJs1quSJc')
								setIsVideoPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_7, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-7.webp'} type="image/webp" />
							<img src={'/images/gallery-7l.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-popup-7.webp')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_8, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-8.webp'} type="image/webp" />
							<img src={'/images/gallery-8.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-8l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>
					<figure className={cn(styles.image_9, styles.image)} >
						<picture className={styles.picture}>
							<source srcSet={'/images/gallery-9.webp'} type="image/webp" />
							<img src={'/images/gallery-9.png'} className={styles.pic} alt='photo' />
						</picture>
						<div className={styles.cover}
							onClick={() => {
								setImgUrl('/images/gallery-9l.png')
								setIsPopupOpened(true)
							}}>
						</div>
					</figure>

				</div>
			</div>

			{isPopupOpened && (
				<div className={cn(styles.popup, {
					[styles.popup_opened]: isPopupOpened
				})}>
					<div className={styles.popup_inner}>
						<img className={styles.popup_img} src={imgUrl} alt="gallery" />
						<button
							onClick={() => setIsPopupOpened(false)}>
							Закрыть
							<img src='images/close.webp' />
						</button>
					</div>
				</div>
			)}

			{isVideoPopupOpened &&
				(<div className={styles.popup}>
					<div className={styles.popup_inner}>
						<iframe className={styles.frame} id="ytplayer" type="text/html"
							src={videoUrl}
							frameBorder="0" />
						<button
							onClick={() => setIsVideoPopupOpened(false)}>
							Закрыть
							<img src='images/close.webp' />
						</button>
					</div>
				</div>)}

		</div>

	);
}

export default Gallery;