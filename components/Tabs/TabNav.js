import React from 'react';
import cn from 'classnames';
import styles from './tab.module.css';


export default class TabNav extends React.Component {

	render() {
		return (
			<div className="tab-container">
				<div className={cn(styles.nav, "nav", "nav-tabs")}>
					{
						this.props.tabs.map(tab => {
							const active = (tab === this.props.selected ? 'active' : '');

							return (
								<div className="nav-tab" key={tab}>
									<a className={"nav-link" + active}
										onClick={() => this.props.setSelected(tab)}>
										{tab}
									</a>
								</div>
							)
						})
					}
				</div>

				{this.props.children}
			</div>
		)
	}
}