import React, { useEffect, useState } from 'react';
import styles from './card.module.css';
import cn from 'classnames';
import Characteristic from '../characteristic/characteristic';
import { useDispatch } from 'react-redux';
import { addItemToCart, incItemCount, delItem, updateItem } from '../../redux/actions/cartActions';
import HeartIcon from '../heart-icon';
import { useSelector } from 'react-redux';

const Card = ({ card }) => {

	const dispatch = useDispatch();
	const { id, title, imgUrl, imgUrlj, price, count, day, characteristics, tag, desc, noDays } = card;

	const cart = useSelector(state => state.catalogState.cart);
	let foundItem = cart.find((found) => found.id === id);

	const daysButtonStatusDefault = day ? day : 1;

	const [daysButtonStatus, setDaysButtonStatus] = useState(daysButtonStatusDefault);
	const [charPopup, setCharPopup] = useState(false);

	const countThePrice = (daysButtonStatus) => {
		switch (daysButtonStatus) {
			case 1:
				return price

			case 2:
				return price + (price * 0.5)

			case 3:
				return price + (price * 0.5) + (price * 0.4)

			case 4:
				return price + (price * 0.5) + (price * 0.4) + (price * 0.35)

			case 5:
				return price + (price * 0.5) + (price * 0.4) + (price * 0.35) + (price * 0.3)

			default:
				return price;
		}
	}

	const x = count === 0 ? 1 : count;
	const priceValue = countThePrice(daysButtonStatus);
	const floorPrice = (Math.floor(priceValue / 100) * 100) * x;
	const priceRu = floorPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').concat(' ₽');

	const checkDesc = desc ? true : false;
	const checkCount = foundItem ? true : false;

	useEffect(() => {
		dispatch(updateItem({
			id,
			tag,
			title,
			imgUrl,
			imgUrlj,
			price,
			total: floorPrice,
			count,
			day,
			noDays,
			desc,
			characteristics
		}))
	}, [floorPrice])

	return (
		<div className={cn(styles.card, {
			[styles.selected]: count > 0
		})}>
			<div className={styles.card_img}>
				<picture>
					<source srcSet={imgUrlj} type="image/webp" />
					<img className={styles.img} src={imgUrl} alt='product' />
				</picture>
				{tag && (
					<div className={styles.tag}>
						{tag}
					</div>
				)}

				<div className={styles.like}>
					<HeartIcon card={card} />
				</div>

			</div>
			<div className={styles.card_inner}>
				<h4>{card.title}</h4>
				<div className={styles.info}>

					{checkDesc || (<a className={styles.link}
						onClick={() => setCharPopup(true)}>Характеристики</a>)}

					{checkDesc && (<p className={styles.desc}>{desc}</p>)}

				</div>

				{noDays || (
					<div className={styles.days}>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 1,
						})} onClick={(() => {
							setDaysButtonStatus(1)
							dispatch(updateItem({
								id,
								tag,
								imgUrl,
								title,
								price,
								total: floorPrice,
								count,
								day: 1,
								noDays,
								desc,
								characteristics
							}))
						})}>1 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 2,
						})} onClick={(() => {
							setDaysButtonStatus(2)
							dispatch(updateItem({
								id,
								tag,
								imgUrl,
								title,
								price,
								total: floorPrice,
								count,
								day: 2,
								noDays,
								desc,
								characteristics
							}))
						})}>2 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 3,
						})} onClick={(() => {
							setDaysButtonStatus(3)
							dispatch(updateItem({
								id,
								tag,
								imgUrl,
								title,
								price,
								total: floorPrice,
								count,
								day: 3,
								noDays,
								desc,
								characteristics
							}))
						})}>3 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 4,
						})} onClick={(() => {
							setDaysButtonStatus(4)
							dispatch(updateItem({
								id,
								tag,
								imgUrl,
								title,
								price,
								total: floorPrice,
								count,
								day: 4,
								noDays,
								desc,
								characteristics
							}))
						})}>4 дн</button>
						<button className={cn(styles.days_button, {
							[styles.days_button_active]: daysButtonStatus == 5,
						})} onClick={() => {
							setDaysButtonStatus(5)
							dispatch(updateItem({
								id,
								tag,
								imgUrl,
								title,
								price,
								total: floorPrice,
								count,
								day: 5,
								noDays,
								desc,
								characteristics
							}))
						}} >5 дн</button>
					</div>
				)}

				<div className={cn(styles.cost, {
					[styles.noDays]: noDays == true
				})}>
					<div className={styles.sum}>
						{priceRu}
					</div>

					{checkCount || (
						<button className={styles.cart}
							onClick={() => {
								dispatch(addItemToCart({
									id,
									imgUrl,
									imgUrlj,
									title,
									price,
									total: floorPrice,
									count: 1,
									day: daysButtonStatus,
									noDays,
									desc,
									characteristics,
									tag
								}))
							}}>

							В корзину <img src='/images/cart-white.svg' alt="cart" />
						</button>
					)}

					{checkCount && (
						<div className={styles.cart_opened}>
							<button onClick={() => {
								{
									count == 1 ? dispatch(delItem({ id })) : dispatch(incItemCount({
										id,
										imgUrl,
										imgUrlj,
										title,
										price,
										total: floorPrice,
										count: count - 1,
										day: daysButtonStatus,
										noDays,
										desc,
										tag,
										characteristics
									}))
								}
							}}>
								<img src="/images/minus.svg" alt="minus" />
							</button>
							<p>{count} шт</p>
							<button onClick={() => {
								dispatch(incItemCount({
									id,
									imgUrl,
									imgUrlj,
									title,
									price,
									total: floorPrice,
									count: count + 1,
									day: daysButtonStatus,
									noDays,
									desc,
									tag,
									characteristics
								}))
							}}>
								<img src="/images/plus.svg" alt="plus" />
							</button>
						</div>
					)}
				</div>
			</div>

			{charPopup && <Characteristic
				id={id}
				imgUrl={imgUrlj}
				characteristics={characteristics}
				setCharPopup={setCharPopup} />}

		</div>
	)
};

export default Card;