/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import styles from './accept-popup.module.css';

const AcceptPopup = ({ setDevPopupStatus, setAcceptPopupStatus }) => {

	const phoneNumber = localStorage.getItem('phoneNumber');

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<img src="/images/checked.svg" alt="checked" />
					<h2 className={styles.title}>Ваша заявка успешно принята в обработку!</h2>
					<p className={styles.paragraph}>Наш менеджер свяжется с вами в течение 10-15 минут
						по указанному номеру {phoneNumber}</p>
					<button className={styles.button} onClick={() => {
						setAcceptPopupStatus(false)
						setDevPopupStatus(true)
					}}>Изменить номер телефона или способ связи</button>

					<button className={styles.close} onClick={() => {
						setAcceptPopupStatus(false)
					}}><img src="/images/close-black.svg" /></button>

				</div>
			</div>
		</div>
	);
}

export default AcceptPopup;