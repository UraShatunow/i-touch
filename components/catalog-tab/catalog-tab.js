import { useState, useEffect } from 'react';
import styles from './catalog-tab.module.css';
import CatalogTabItem from '../catalog-tabs/catalog-tab-item';
import CatalogTabNav from '../catalog-tabs/catalog-tab-nav';
import Card from '../card/card';
import cn from 'classnames';
import Arrow from '../arrow/arrow';
import { useSelector, useDispatch } from 'react-redux';

const CatalogTab = () => {


	const cart = useSelector(state => state.catalogState.cart);
	const cards = useSelector(state => state.catalogState.cards);

	const [content, setContent] = useState(false);
	const [defaultButton, setDefaultButton] = useState('Брендирование оборудования');
	const [isMenuSelected, setIsMenuSelected] = useState(false);
	const [selected, setSelected] = useState('Брендирование оборудования');

	const initSetSelected = (tab) => {
		setSelected(tab);
	}

	const createCards = (item) => {
		let foundItem = cart.find((found) => found.id === item.id);

		if (foundItem) {
			return (<Card
				key={foundItem.id}
				card={foundItem} />)
		}

		return (<Card
			key={item.id}
			card={item} />)
	}


	const plasmaCards = cards.filter(i => i.type == 'plasma').map((item) => { return createCards(item) });
	const brandCards = cards.filter(i => i.type == 'brand').map((item) => { return createCards(item) });
	const stoikiCards = cards.filter(i => i.type == 'stoiki').map((item) => { return createCards(item) });
	const tableCards = cards.filter(i => i.type == 'table').map((item) => { return createCards(item) });
	const lightCards = cards.filter(i => i.type == 'light').map((item) => { return createCards(item) });
	const desinfCards = cards.filter(i => i.type == 'desinf').map((item) => { return createCards(item) });
	const personalCards = cards.filter(i => i.type == 'personal').map((item) => { return createCards(item) });

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>

					<h2 className={styles.title}>Сделайте мероприятие ещё лучше</h2>

					<CatalogTabNav tabs={['Брендирование оборудования',
						'Аренда плазмы',
						'Стойки и экспостенды',
						'Таблички-указатели',
						'Светодиодная подсветка',
						'Дезинфекция',
						'Сопровождающий персонал']}
						selected={selected}
						initSetSelected={initSetSelected} >

						<CatalogTabItem isSelected={selected === 'Брендирование оборудования'}>
							<div className={styles.catalog_tab_inner}>
								{brandCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Аренда плазмы'}>
							<div className={styles.catalog_tab_inner}>
								{plasmaCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Стойки и экспостенды'}>
							<div className={styles.catalog_tab_inner}>
								{stoikiCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Таблички-указатели'}>
							<div className={styles.catalog_tab_inner}>
								{tableCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Светодиодная подсветка'}>
							<div className={styles.catalog_tab_inner}>
								{lightCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Дезинфекция'}>
							<div className={styles.catalog_tab_inner}>
								{desinfCards}
							</div>
						</CatalogTabItem>

						<CatalogTabItem isSelected={selected === 'Сопровождающий персонал'}>
							<div className={styles.catalog_tab_inner}>
								{personalCards}
							</div>
						</CatalogTabItem>

					</CatalogTabNav>

					<div className={styles.inner_small}>

						<div className={styles.menu}>
							<button className={styles.menu_default}
								onClick={() => setIsMenuSelected(!isMenuSelected)}>{defaultButton}
								<Arrow
									arrow={isMenuSelected ? 'up' : 'down'}
									color={'white'} />
							</button>
							<div className={cn(styles.menu_list, {
								[styles.active]: isMenuSelected
							})}>
								<button className={styles.menu_button} onClick={() => {
									setContent(brandCards)
									setDefaultButton('Брендирование оборудования')
									setIsMenuSelected(false)
								}}>Брендирование оборудования</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(plasmaCards)
									setDefaultButton('Аренда плазмы')
									setIsMenuSelected(false)
								}}>Аренда плазмы</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(stoikiCards)
									setDefaultButton('Стойки и экспостенды')
									setIsMenuSelected(false)
								}}>Стойки и экспостенды</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(tableCards)
									setDefaultButton('Таблички-указатели')
									setIsMenuSelected(false)
								}}>Таблички-указатели</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(lightCards)
									setDefaultButton('Светодиодная подсветка')
									setIsMenuSelected(false)
								}}>Светодиодная подсветка</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(desinfCards)
									setDefaultButton('Дезинфекция')
									setIsMenuSelected(false)
								}}>Дезинфекция</button>
								<button className={styles.menu_button} onClick={() => {
									setContent(personalCards)
									setDefaultButton('Сопровождающий персонал')
									setIsMenuSelected(false)
								}}>Сопровождающий персонал</button>
							</div>
						</div>

						<div className={styles.content_small}>
							{content || brandCards}
						</div>

					</div>

				</div>
			</div>
		</div>
	);
}

export default CatalogTab;