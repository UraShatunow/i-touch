import React, { useState } from 'react';
import styles from './request.module.css';
import { IMaskInput } from 'react-imask';
import Arrow from '../arrow/arrow';
import cn from 'classnames';
import AcceptPopup2 from '../accept-popup-2';

const Request = () => {

	const [popupStatus, setPopupStatus] = useState(false);
	const [selectorValue, setSelectorValue] = useState('Позвонить по телефону');
	const [acceptPopupStatus, setAcceptPopupStatus] = useState(false);
	const onHandleInputChange = (e) => {
		localStorage.setItem('phoneNumber', e.target.value);
	}

	return (
		<div className={styles.content}>
			<div className={styles.container}>
				<div className={styles.inner}>
					<h2>Оставьте заявку и наш менеджер свяжется с вами</h2>
					<form className={styles.form} name="register"
						onSubmit={(event) => {
							event.preventDefault()
							setAcceptPopupStatus(true)
						}}>
						<IMaskInput
							mask={'+{7}(000)000-00-00'}
							placeholder="+ 7 (___)-___-__-__"
							pattern="^(\+7|8)((\(\d{3}\)\s?\d{3}-\d{2}-\d{2})|(\s\d{3}-\d{3}-\d{2}-\d{2})|(\d{10}))$"
							minLength="11"
							maxLength="18"
							required
							onChange={onHandleInputChange}
						/>

						<div className={styles.selector}>
							<button className={cn(styles.selector_default, {
								[styles.opened]: popupStatus == true
							})} type='button'
								onClick={() => {
									setPopupStatus(!popupStatus)
								}}>
								{selectorValue}
								<Arrow
									arrow={popupStatus ? 'up' : 'down'}
									color={popupStatus ? 'white' : 'black'} />
							</button>

							{popupStatus && (
								<div className={styles.selector_list}>
									<button className={styles.selector_item}
										onClick={() => {
											setSelectorValue('Позвонить по телефону')
											setPopupStatus(false)
										}}>
										Позвонить по телефону
									</button>
									<button className={styles.selector_item}
										onClick={() => {
											setSelectorValue('Написать в WhatsApp')
											setPopupStatus(false)
										}}>
										Написать в WhatsApp
									</button>
									<button className={styles.selector_item}
										onClick={() => {
											setSelectorValue('Написать в Telegram')
											setPopupStatus(false)
										}}>
										Написать в Telegram
									</button>
								</div>
							)}

						</div>
						<button className={styles.submit} type="submit">Получить консультацию</button>
					</form>
				</div>
			</div>

			{acceptPopupStatus && <AcceptPopup2
				setAcceptPopupStatus={setAcceptPopupStatus} />}

		</div>
	);
}

export default Request;