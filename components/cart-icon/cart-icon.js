import React from 'react';
import Link from 'next/link';
import styles from './cart-icon.module.css';
import { useSelector } from 'react-redux';

const CartIcon = () => {

	const cart = useSelector(state => state.catalogState.cart);

	let count = 0;

	cart.forEach((item) => {
		return count = count+ item.count;
	})

	return (
		<Link href="/cart" passHref>
			<a href="passed" className={styles.content}>
				<img src="/images/cart.svg" alt="cart" className={styles.img} />
				{count > 0 && (<span className={styles.count}>{count}</span>)}
			</a>
		</Link>
	);
}

export default CartIcon;