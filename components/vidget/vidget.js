import React, { useState } from 'react';
import styles from './vidget.module.css';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import DevPopup from '../dev-popup';
import AcceptPopup from '../accept-popup';

const Vidget = () => {

	const cart = useSelector(state => state.catalogState.cart);
	const [devPopupStatus, setDevPopupStatus] = useState(false);
	const [acceptPopupStatus, setAcceptPopupStatus] = useState(false);

	let sum = 0;

	cart.forEach((item) => {
		return sum = sum + item.total;
	})

	const check = cart.length ? true : false;

	const priceRu = sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ').concat(' ₽');

	return (

		<>
			{check && (

				<div className={styles.content}>
					<p>{priceRu}</p>
					<button href="passed" className={styles.button} onClick={() => {
						setDevPopupStatus(true)
					}}>Арендовать</button>
				</div>

			)}

			{devPopupStatus && <DevPopup
				setDevPopupStatus={setDevPopupStatus}
				setAcceptPopupStatus={setAcceptPopupStatus}
				title={'Заявка на аренду'} />}
			{acceptPopupStatus && <AcceptPopup
				setDevPopupStatus={setDevPopupStatus}
				setAcceptPopupStatus={setAcceptPopupStatus} />}

		</>

	);
}


export default Vidget;