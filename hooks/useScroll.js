import { useState, useEffect } from "react";

const useScroll = () => {

	const check = typeof document == 'undefined' ? 0 : document.body.getBoundingClientRect()
	const [lastScrollTop, setLastScrollTop] = useState(0);
	const [bodyOffset, setBodyOffset] = useState(
		check
	);
	const [scrollY, setScrollY] = useState(bodyOffset.top);
	const [scrollX, setScrollX] = useState(bodyOffset.left);
	const [scrollDirection, setScrollDirection] = useState();

	const listener = e => {
		setBodyOffset(check);
		setScrollY(-bodyOffset.top);
		setScrollX(bodyOffset.left);
		setScrollDirection(lastScrollTop > -bodyOffset.top ? "down" : "up");
		setLastScrollTop(-bodyOffset.top);
	};

	useEffect(() => {
		window.addEventListener("scroll", listener);
		return () => {
			window.removeEventListener("scroll", listener);
		};
	});

	return {
		scrollY,
		scrollX,
		scrollDirection
	};
}

export default useScroll;