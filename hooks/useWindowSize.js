import React, { useState, useEffect } from 'react';

const useWindowSize = () => {
	const [width, setWindowWidth] = useState(undefined);

	useEffect(() => {
		if (typeof window !== 'undefined') {
			const handleResize = () => {
				setWindowWidth(window.screen.width);
			}

			window.addEventListener("resize", handleResize);
			handleResize();
			return () => window.removeEventListener("resize", handleResize);
		}
	}, []);
	return width;
}

export default useWindowSize;