export default class CardServiсe {

	cards = [
		{
			id: 1,
			title: 'Интерактивный стол 43 дюйма',
			count: 0,
			price: 14300,
			imgUrl: '/images/cards/catalog-1.webp',
			imgUrlj: '/images/cards/catalog-1.jpg',
			type: 'main',
			tag: 'хит',
			characteristics: [
				{ info: 'Материал корпуса: Сталь' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 2,
			title: 'Интерактивный стол 50 дюймов',
			count: 0,
			price: 16300,
			imgUrl: '/images/cards/catalog-1.webp',
			imgUrlj: '/images/cards/catalog-1.jpg',
			type: 'main',
			tag: 'хит',
			characteristics: [
				{ info: 'Материал корпуса: Дерево' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 3,
			title: 'Интерактивный стол 43 дюйма',
			count: 0,
			price: 23300,
			imgUrl: '/images/cards/catalog-3.webp',
			imgUrlj: '/images/cards/catalog-3.jpg',
			type: 'main',
			tag: 'хит',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 4,
			title: 'Интерактивный стол 55 дюймов',
			count: 0,
			price: 34800,
			imgUrl: '/images/cards/catalog-4.webp',
			imgUrlj: '/images/cards/catalog-4.jpg',
			type: 'main',
			tag: 'престиж',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 5,
			title: 'Интерактивный стол 43 дюйма',
			count: 0,
			price: 15500,
			imgUrl: '/images/cards/catalog-5.webp',
			imgUrlj: '/images/cards/catalog-5.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 6,
			title: 'Интерактивный киоск 32 дюйма',
			count: 0,
			price: 16200,
			imgUrl: '/images/cards/catalog-6.webp',
			imgUrlj: '/images/cards/catalog-6.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 7,
			title: 'Интерактивный стол 43 дюйма',
			count: 0,
			price: 14200,
			imgUrl: '/images/cards/catalog-7.webp',
			imgUrlj: '/images/cards/catalog-7.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 8,
			title: 'Интерактивный стол 50 дюймов',
			count: 0,
			price: 19800,
			imgUrl: '/images/cards/catalog-7.webp',
			imgUrlj: '/images/cards/catalog-7.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 9,
			title: 'Интерактивный стол 32 дюйма',
			count: 0,
			price: 15000,
			imgUrl: '/images/cards/catalog-8.webp',
			imgUrlj: '/images/cards/catalog-8.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 10,
			title: 'Интерактивный киоск 19 дюймов',
			count: 0,
			price: 8000,
			imgUrl: '/images/cards/catalog-9.webp',
			imgUrlj: '/images/cards/catalog-9.jpg',
			type: 'main',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 11,
			title: 'Интерактивный стол 32 дюйма',
			count: 0,
			price: 10300,
			imgUrl: '/images/cards/catalog-11.webp',
			imgUrlj: '/images/cards/catalog-11.jpg',
			type: 'main',
			tag: 'эко',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 12,
			title: 'Интерактивный киоск 32 дюйма',
			count: 0,
			price: 10800,
			imgUrl: '/images/cards/catalog-12.webp',
			imgUrlj: '/images/cards/catalog-12.jpg',
			type: 'main',
			tag: 'эко',
			characteristics: [
				{ info: 'Материал корпуса: Папирус' },
				{ info: 'Диагональ Экрана: 32' },
				{ info: 'Разрешение: FullHD' },
				{ info: 'Сенсор: 10 одновременных касаний' },
				{ info: 'Процессор: AMD' },
				{ info: 'Оперативная память: 4' },
				{ info: 'Жесткий диск: SSD 120 гб' },
				{ info: 'WI-FI адаптер: есть' },
				{ info: 'Аудиосистема: Встроенная 2x5 Вт' },
				{ info: 'Операционная система: Windows' },
				{ info: 'Цвет: Чёрный' },
				{ info: 'Внешние разъемы: USB 3.0; USB 2.0; RJ-45; AUX' }
			]
		},
		{
			id: 13,
			title: 'Плазма 50 дюймов',
			count: 0,
			price: 3000,
			imgUrl: '/images/cards/plasma-1.webp',
			imgUrlj: '/images/cards/plasma-1.jpg',
			type: 'plasma',
			desc: 'Размеры: 1126мм х 652мм'
		},
		{
			id: 14,
			title: 'Плазма 55 дюйма',
			count: 0,
			price: 3600,
			imgUrl: '/images/cards/plasma-2.webp',
			imgUrlj: '/images/cards/plasma-2.jpg',
			type: 'plasma',
			desc: 'Размеры: 1243мм х 730мм'
		},
		{
			id: 15,
			title: 'Плазма 60 дюймов',
			count: 0,
			price: 4200,
			imgUrl: '/images/cards/plasma-3.webp',
			imgUrlj: '/images/cards/plasma-3.jpg',
			type: 'plasma',
			desc: 'Размеры: 1350мм х 796мм'
		},
		{
			id: 16,
			title: 'Дизайн макета для обклейки',
			count: 0,
			price: 4200,
			imgUrl: '/images/cards/brand-1.webp',
			imgUrlj: '/images/cards/brand-1.jpg',
			type: 'brand',
			tag: 'хит',
			desc: 'Разработаем макет по вашему заказу',
			noDays: true
		},
		{
			id: 17,
			title: 'Брендирование Лайт',
			count: 0,
			price: 5800,
			imgUrl: '/images/cards/brand-2.webp',
			imgUrlj: '/images/cards/brand-2.jpg',
			type: 'brand',
			desc: 'Обклейка до 50% стола',
			noDays: true
		},
		{
			id: 18,
			title: 'Брендирование Тотал',
			count: 0,
			price: 9800,
			imgUrl: '/images/cards/brand-3.webp',
			imgUrlj: '/images/cards/brand-3.jpg',
			type: 'brand',
			tag: 'престиж',
			desc: 'Обклейка до 100% стола',
			noDays: true
		},
		{
			id: 19,
			title: 'Стойка под плазму',
			count: 0,
			price: 500,
			imgUrl: '/images/cards/stoiki-1.webp',
			imgUrlj: '/images/cards/stoiki-1.jpg',
			type: 'stoiki',
			desc: '1,5м, с держателем'
		},
		{
			id: 20,
			title: 'Стойка с плашкой',
			count: 0,
			price: 4800,
			imgUrl: '/images/cards/stoiki-2.webp',
			imgUrlj: '/images/cards/stoiki-2.jpg',
			type: 'stoiki',
			desc: 'Возможно брендирование'
		},
		{
			id: 21,
			title: 'Экспостенд под плазму',
			count: 0,
			price: 9500,
			imgUrl: '/images/cards/stoiki-3.webp',
			imgUrlj: '/images/cards/stoiki-3.jpg',
			type: 'stoiki',
			desc: 'Возможно брендирование'
		},
		{
			id: 22,
			title: 'Табличка-указатель ',
			count: 0,
			price: 900,
			imgUrl: '/images/cards/table-1.webp',
			imgUrlj: '/images/cards/table-1.jpg',
			type: 'table',
			desc: 'Для помещений'
		},
		{
			id: 23,
			title: 'Табличка-указатель',
			count: 0,
			price: 1500,
			imgUrl: '/images/cards/table-2.webp',
			imgUrlj: '/images/cards/table-2.jpg',
			type: 'table',
			desc: 'Ветроустойчивый'
		},
		{
			id: 24,
			title: 'Инфоромационный стенд',
			count: 0,
			price: 9000,
			imgUrl: '/images/cards/table-3.webp',
			imgUrlj: '/images/cards/table-3.jpg',
			type: 'table',
			desc: 'Установка на улице либо в помещении'
		},
		{
			id: 25,
			title: 'Светодиодный прожектор',
			count: 0,
			price: 1300,
			imgUrl: '/images/cards/light-1.webp',
			imgUrlj: '/images/cards/light-1.jpg',
			type: 'light',
			desc: 'Напольный'
		},
		{
			id: 26,
			title: 'Светодиодная подсветка',
			count: 0,
			price: 1500,
			imgUrl: '/images/cards/light-2.webp',
			imgUrlj: '/images/cards/light-2.jpg',
			type: 'light',
			desc: 'Линейная, напольная'
		},
		{
			id: 27,
			title: 'Светодиодная лампа на стойке',
			count: 0,
			price: 2000,
			imgUrl: '/images/cards/light-3.webp',
			imgUrlj: '/images/cards/light-3.jpg',
			type: 'light',
			desc: 'Цена за ед.'
		},
		{
			id: 28,
			title: 'Сенсорный санитайзер',
			count: 0,
			price: 1900,
			imgUrl: '/images/cards/desinf-1.webp',
			imgUrlj: '/images/cards/desinf-1.jpg',
			type: 'desinf',
			desc: 'До 200 применений'
		},
		{
			id: 29,
			title: 'Антивандальный санитайзер',
			count: 0,
			price: 3800,
			imgUrl: '/images/cards/desinf-2.webp',
			imgUrlj: '/images/cards/desinf-2.jpg',
			type: 'desinf',
			desc: 'C механической педалью'
		},
		{
			id: 30,
			title: 'Дополнительный раствор',
			count: 0,
			price: 1800,
			imgUrl: '/images/cards/desinf-3.webp',
			imgUrlj: '/images/cards/desinf-3.jpg',
			type: 'desinf',
			desc: 'До 200 дополнительных доз (5л)',
			noDays: true
		},
		{
			id: 31,
			title: 'Промоутер',
			count: 0,
			price: 4000,
			imgUrl: '/images/cards/personal-1.webp',
			imgUrlj: '/images/cards/personal-1.png',
			type: 'personal',
			desc: 'Цена за 3 часа, доп.час +500₽',
			noDays: true
		},
		{
			id: 32,
			title: 'Инструктор',
			count: 0,
			price: 4000,
			imgUrl: '/images/cards/personal-2.webp',
			imgUrlj: '/images/cards/personal-2.jpg',
			type: 'personal',
			desc: 'Цена за 6 часов',
			noDays: true
		},
		{
			id: 33,
			title: 'Аниматор / Ведущий',
			count: 0,
			price: 6000,
			imgUrl: '/images/cards/personal-3.webp',
			imgUrlj: '/images/cards/personal-3.jpg',
			type: 'personal',
			desc: 'Цена за 3 часа, доп.час +1000₽',
			noDays: true
		}
	];

	getCards() {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if (Math.random() > 1) {
					reject(new Error('Something bad happened'));
				} else {
					resolve(this.cards);
				}
			}, 0);
		});
	}

}